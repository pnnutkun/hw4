package ku;

import java.time.LocalDate;
import java.util.*;

/**
 * A student with an id, name, sex, and birthday.
 * @author jim
 */
public class Student {
	private String id;
	private String name;
	private LocalDate birthday;
	private char sex; // 'M' for male, 'F' for female
	
	/** initialize a new Student with a name and birthday */
	public Student(String name, char sex, LocalDate birthday) {
		this("",name,sex, birthday);
	}

	public Student(String id, String name, char sex, LocalDate birthday) {
		this.id = id;
		this.name = name;
		this.sex = sex;
		this.birthday = birthday;
	}
	
	/**
	 * @return the name of this student
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the id of this student
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the birthday of this student
	 */
	public LocalDate getBirthday() {
		return birthday;
	}
	
	/**
	 * Return the object's sex as a char. 'M' for male, 'F' for female.
	 * @return this object's sex attribute
	 */
	public char getSex() {
		return sex;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("%s [%s] Born %s", name, id, birthday.toString() );
	}
}
